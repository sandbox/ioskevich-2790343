<?php

/**
 * Implements hook_ctools_plugin_directory().
 */
function webhook_jira_ctools_plugin_directory($module, $type) {
  if ('webhook' == $module) {
    return "plugins/{$type}";
  }
}

/**
 * Implements hook_webhook_processor().
 */
function webhook_jira_webhook_processor() {

  $path = drupal_get_path('module', 'webhook_jira') . '/plugins/processor';
  $plugins = array();

  $plugins['jirasync'] = array(
    'title' => t('Jira Issues Sync'),
    'processor' => array(
      'path' => $path,
      'file' => 'Webhook_Jira_Plugins_Processor_JiraSync.class.inc',
      'class' => 'Webhook_Jira_Plugins_Processor_JiraSync',
    ),
  );

  return $plugins;
}

/**
 * Implements hook_webhook_unserializer().
 */
function webhook_jira_webhook_unserializer() {

  $path = drupal_get_path('module', 'webhook_jira') . '/plugins/unserializer';
  $plugins = array();

  $plugins['jira'] = array(
    'title' => t('Jira'),
    'unserializer' => array(
      'path' => $path,
      'file' => 'Webhook_Jira_Plugins_Unserializer_Jira.class.inc',
      'class' => 'Webhook_Jira_Plugins_Unserializer_Jira',
    ),
  );

  return $plugins;
}

/**
* Implements hook_token_info().
*/
function webhook_jira_token_info() {
  $info['types']['webhook_jira_data'] = array(  // type declared in hook_rules_data_info().
    'name' => t('Jira webhook data.'), 
    'description' => t('Tokens for the data received from Jira webhook.'), 
  );
  $jira_data_properties = _webhook_jira_data_properties();
  foreach ($jira_data_properties as $key => $value) {
    $info['tokens']['webhook_jira_data'][$key] = array(
      'name' => $key, 
      'description' => $value['label'],
    );
  }
  return $info;
}


/**
 * Implements hook_tokens().
 */
function webhook_jira_tokens($type, $tokens, array $data = array(), array $options = array()) {
  if ($type == 'webhook_jira_data') {
    if(isset($data['webhook_jira_data'])) {
      $replacements = array();
      $mydata = $data['webhook_jira_data'];
      foreach ($tokens as $name => $original) {
        $replacements[$original] = $mydata[$name];
      }
    }
    return $replacements;
  }
}


/**
 * Helper function to defines data properties available from the POST data 
 * sent by Jira to Webhook module endpoint.
 */
function _webhook_jira_data_properties() {
  return array(
    'jira_issue_id' => array(
      'type' => 'integer',
      'label' => t('Jira issue ID'),
    ),
    'jira_issue_key' => array(
      'type' => 'text',
      'label' => t('Jira issue key'),
      'sanitized' => TRUE,
    ),
    'jira_issue_api_url' => array(
      'type' => 'text',
      'label' => t('Jira issue API URL'),
    ),
    'jira_issue_summary' => array(
      'type' => 'text',
      'label' => t('Jira issue title'),
      'sanitized' => TRUE,
    ),
    'jira_issue_description' => array(
      'type' => 'text',
      'label' => t('Jira issue description'),
      'sanitized' => TRUE,
    ),
    'jira_issue_priority_name' => array(
      'type' => 'text',
      'label' => t('Jira issue priority name'),
      'sanitized' => TRUE,
    ),
    'jira_issue_priority_id' => array(
      'type' => 'integer',
      'label' => t('Jira issue priority ID'),
    ),
    'jira_user_name' => array(
      'type' => 'text',
      'label' => t('Jira user name'),
      'sanitized' => TRUE,
    ),
    'jira_user_email' => array(
      'type' => 'text',
      'label' => t('Jira user email'),
      'sanitized' => TRUE,
    ),
    'jira_webhook_event' => array(
      'type' => 'text',
      'label' => t('Jira webhook event'),
      'sanitized' => TRUE,
    ),
    'jira_comment_id' => array(
      'type' => 'integer',
      'label' => t('Jira comment ID'),
    ),
    'jira_comment_author_name' => array(
      'type' => 'text',
      'label' => t('Jira comment author name'),
      'sanitized' => TRUE,
    ),
    'jira_comment_author_email' => array(
      'type' => 'text',
      'label' => t('Jira comment author email'),
    ),
    'jira_comment_body' => array(
      'type' => 'text',
      'label' => t('Jira comment content'),
      'sanitized' => TRUE,
    ),
  );
}