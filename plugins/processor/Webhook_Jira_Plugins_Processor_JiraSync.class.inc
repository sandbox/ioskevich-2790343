<?php

/**
 * @file
 * Jira plugin class to sync Jira data to Drupal entities.
 */

/**
 * Webhook Processor Jira Sync class.
 *
 */
class Webhook_Jira_Plugins_Processor_JiraSync implements Webhook_Plugins_Processor_Interface {

  /**
   * Configuration form.
   */
  public function config_form() {
    // No configuration needed.
    return array();
  }

  /**
   * Returns array structure that can be consumed by Rules.
   * @see _webhook_jira_data_properties() in .module file.
   */
  public function process(stdClass $data) {
    $webhook_data = array(
      'type' => 'jira_webhook_data',
      'jira_issue_id' => $data->issue->id,
      'jira_issue_key' => $data->issue->key,
      'jira_issue_api_url' => $data->issue->self,
      'jira_issue_summary' => $data->issue->fields->summary,
      'jira_issue_description' => $data->issue->fields->description,
      'jira_issue_priority_id' => $data->issue->fields->priority->id,
      'jira_issue_priority_name' => $data->issue->fields->priority->name,
      'jira_user_name' => $data->user->name,
      'jira_user_email' => $data->user->emailAddress,
      'jira_webhook_event' => $data->webhookEvent,
    );

    if (isset($data->comment)) {
      $webhook_data['jira_comment_id'] = $data->comment->id;
      $webhook_data['jira_comment_author_name'] = $data->comment->author->name;
      $webhook_data['jira_comment_author_email'] = $data->comment->author->emailAddress;
      $webhook_data['jira_comment_body'] = $data->comment->body;
    }

    $message = 'Jira issue updated: @issue_key. Priority: @priority_name (@priority_id).';
    
    $vars = array(
      '@issue_key' => $webhook_data['jira_issue_key'],
      '@priority_name' => $webhook_data['jira_issue_priority_name'],
      '@priority_id' => $webhook_data['jira_issue_priority_id'],
    );

    rules_invoke_event('webhook_jira_triggered', $webhook_data); 

    return watchdog('jira_sync', $message, $vars, WATCHDOG_NOTICE);
  }
}
