<?php

/**
 * @file
 * Example plugin class for unserializing github commit hook data.
 */

/**
 * Webhook unserializer plugin for Github post receive data.
 */
class Webhook_Jira_Plugins_Unserializer_Jira implements Webhook_Plugins_Unserializer_Interface {
  public function unserialize($data) {
    $json = webhook_load_unserializer('json');
    return $json->unserialize($data);
  }
}
