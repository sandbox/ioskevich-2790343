<?php

/**
 * Implementation of hook_rules_event_info().
 * @ingroup rules
 */
function webhook_jira_rules_event_info() {
  return array(
    'webhook_jira_triggered' => array(
      'label' => t('Jira webhook is triggered'),
      'group' => 'Jira Webhooks',
      'module' => 'webhook_jira',
      'variables' => array(
        'webhook_data' => array(
          'label' => t('Received webhook data'),
          'type' => 'webhook_jira_data',
          //'bundle' => 'jira_webhook_data'
        ),
      ),
    ),
  );
}

/**
 * Implements hook_rules_data_info().
 */
function webhook_jira_rules_data_info() {
  return array(
    'webhook_jira_data' => array(
      'label' => t('Jira webhook data.'),
      'wrap' => TRUE,
      'property info' => _webhook_jira_data_info(),
    ),
  );
}

/**
 * Defines property info for the POST data sent by Jira to Webhook module endpoint.
 */
function _webhook_jira_data_info() {
  return _webhook_jira_data_properties();
}
