<?php
/**
 * @file
 * webhook_jira.webhook.inc
 */

/**
 * Implements hook_webhook_default_config().
 */
function webhook_jira_webhook_default_config() {
  $export = array();

  $webhook = new stdClass();
  $webhook->disabled = FALSE; /* Edit this to true to make a default webhook disabled initially */
  $webhook->api_version = 1;
  $webhook->whid = '1';
  $webhook->title = 'Jira';
  $webhook->machine_name = 'jira';
  $webhook->description = 'Jira webhook for syncing Jira issues data.';
  $webhook->unserializer = 'jira';
  $webhook->processor = 'jirasync';
  $webhook->config = '';
  $webhook->enabled = TRUE;
  $export['jira'] = $webhook;

  return $export;
}
